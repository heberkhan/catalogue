<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CartController;
use App\Http\Controllers\API\ProductController;
use App\Http\Controllers\API\CategoryController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('products', ProductController::class);

Route::apiResource('categories', CategoryController::class);

// Routes shoppin cart
Route::post('add', [CartController::class ,'add']);
Route::get('cart', [CartController::class ,'index']);
Route::post('cart/update', [CartController::class ,'update']);
Route::post('cart/remove', [CartController::class ,'remove']);
Route::post('cart/clear', [CartController::class ,'clear']);
