## how to install

- Clone the repository
- composer install
- php artisan migrate --seed // to perform the seed in the database
- php artisan serve // to run project

//this api must be consumed from a postman application

## Credits
- Project: Api catalogue
- Developed by: Heber Andrés Hernández Velásquez
- email: heberkhan@gmail.com
- phone: +57 3223675019
- LinkedIn: https://www.linkedin.com/in/heber-andres-hernandez-velasquez-a305b3a6/
