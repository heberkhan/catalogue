<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;
use GuzzleHttp\Client;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // the query is made to the currencyfreaks api through guzzle
        $client = new Client();
        $response = $client->request('GET', 'https://api.currencyfreaks.com/latest?apikey=ab0e62b1751240de8aeb1d797fea371e');

        $rta = json_decode($response->getBody()->getContents());
        // the result is multiplied with the value of the product already entered
        $priceCOP = $rta->rates->COP * $this->price;

        // the values ​​are returned in this format ready for viewing
        return [
            'id' => $this->id,
            'name' => Str::upper($this->name),
            'description' => Str::upper($this->description),
            'weight' => $this->weight,
            'priceUSD' => '$'.$this->price,
            'priceCOP' => '$'.$priceCOP,
        ];
    }

    public function with($request)
    {
        return [
            'res' => true,
        ];
    }
}
