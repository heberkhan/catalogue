<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    
    public function index()
    {
        $cartCollection = \Cart::getContent();
        $cart = json_decode($cartCollection);
        // dd($cart);
        return response()->json(
            [
                'res' => true,
                'cart'=> $cart
            ], 200);
    }

    // add item to cart
    public function add(Request $request){
        $product = Product::find($request->id);
        \Cart::add(
            $product->id,
            $product->name,
            $product->description,
            $product->price,
            $product->weight,
            $request->quantity,
            
        );
        return response()->json(
            [
                'res' => true,
                'msg' => 'Item is Added to Cart!',
            ], 200
        );
    }
    // remove items to cart
    public function remove(Request $request){
        \Cart::remove($request->id);
        return response()->json(
            [
                'res' => true,
                'msg' => 'Item is removed to Cart!',
            ], 200
        );
    }
    // update item in cart
    public function update(Request $request){
        \Cart::update($request->id,
            array(
                'quantity' => array(
                    'relative' => false,
                    'value' => $request->quantity
                ),
        ));
        return response()->json(
            [
                'res' => true,
                'msg' => 'Item is updated to Cart!',
            ], 200
        );
    }
    // clear the cart
    public function clear(){
        \Cart::clear();
        return response()->json(
            [
                'res' => true,
                'msg' => 'Car is cleared!',
            ], 200
        );
    }
}
