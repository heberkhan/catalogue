<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use Intervention\Image\ImageManagerStatic as Image;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CategoryResource::collection(Category::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {
        $category = new Category;
        $category->name = $request->name;
        $category->category = $request->category;
        $category->save();


        if ($request->hasFile('photo'))
        {
            if (!File::exists(public_path().'/category/image'))
            {
                File::makeDirectory(public_path().'/category/image');
            }

            $image = $request->file('photo');
            $name = 'category'.$category->id .'-img.'.$image->getClientOriginalExtension();
            $destinationPath = public_path().'/category/image';
            $imagePath = $destinationPath ."/".$name;
            // CREACIÓN Y RESIZE DE LA IMG
            $img = Image::make($image);
            $canvas = Image::canvas(50, 50);
            $img->resize(50, 50, function ($constraint)
            {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $canvas->insert($img, 'center');
            $canvas->save($imagePath, 100);
            $imageBd = 'category/image/'.$name;
            
            $category->photo = $imageBd;
            $category->save();

        }
        return response()
                ->json([
                    'res' => true,
                    'msg' => 'Category created successfully'
                ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return new CategoryResource($category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request, Category $category)
    {
        
        $category->name = $request->name;
        $category->category = $request->category;
        $category->save();


        if ($request->hasFile('photo'))
        {
            if (!File::exists(public_path().'/category/image'))
            {
                File::makeDirectory(public_path().'/category/image');
            }

            $image = $request->file('photo');
            $name = 'category'.$category->id .'-img.'.$image->getClientOriginalExtension();
            $destinationPath = public_path().'/category/image';
            $imagePath = $destinationPath ."/".$name;
            // CREACIÓN Y RESIZE DE LA IMG
            $img = Image::make($image);
            $canvas = Image::canvas(50, 50);
            $img->resize(50, 50, function ($constraint)
            {
                $constraint->aspectRatio();
                $constraint->upsize();
            });
            $canvas->insert($img, 'center');
            $canvas->save($imagePath, 100);
            $imageBd = 'category/image/'.$name;
            
            $category->photo = $imageBd;
            $category->save();

        }
        
        return (new CategoryResource($category))
                ->additional(['msg' => 'Category updated successfully'])
                ->response()
                ->setStatusCode(202);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return (new CategoryResource($category))->additional(['msg' => 'Category deleted successfully']);
    }

    
}
