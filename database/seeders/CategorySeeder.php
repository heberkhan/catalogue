<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	[
	        	'name' => 'Technology',
	        	'photo' => 'store/categories/photo',
	        	'category' => 'tech'	        	
	        	
        	],
            [
	        	'name' => 'Home',
	        	'photo' => 'store/categories/photo',
	        	'category' => 'home'	        	
	        	
        	],
            [
	        	'name' => 'Sports',
	        	'photo' => 'store/categories/photo',
	        	'category' => 'sprt'	        	
	        	
        	],
            
        	
        	
        ]);
    }
}
