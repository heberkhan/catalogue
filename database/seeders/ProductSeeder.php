<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
        	[
	        	'name' => 'Televisor Samsung',
	        	'description' => '60 pulgadas, LED, 4k',
	        	'weight' => 28,
	        	'price' => 2500000,
	        	
        	],
            [
	        	'name' => 'Computador Acer',
	        	'description' => 'Intel Core I9, 16 gb Ram, SSD 500GB',
	        	'weight' => 15,
	        	'price' => 5000000,
	        	
        	],
            [
	        	'name' => 'Teclado Gamer',
	        	'description' => 'Mecánico, RGB',
	        	'weight' => 5,
	        	'price' => 70000,
	        	
        	],
        	
        	
        ]);
    }
}
